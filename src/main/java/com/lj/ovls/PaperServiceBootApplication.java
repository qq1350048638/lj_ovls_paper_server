package com.lj.ovls;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;

@EnableFeignClients
@EnableDiscoveryClient
@SpringBootApplication
@ServletComponentScan
@MapperScan(basePackages={"com.lj.ovls.paper.dao"})
public class PaperServiceBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(PaperServiceBootApplication.class, args);
	}

}
