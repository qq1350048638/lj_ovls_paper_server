package com.lj.ovls.paper.config;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class RestTemplateConfig {

	@Bean
	@LoadBalanced//启用ribbon负载均衡调用服务
	public RestTemplate cteateRestTemplate(){
		return new RestTemplate();
	}
	
}
