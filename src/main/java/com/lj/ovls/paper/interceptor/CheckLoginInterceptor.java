package com.lj.ovls.paper.interceptor;

import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;


import com.lj.ovls.common.entity.ResponseResult;
import com.lj.ovls.paper.remote.UserServiceRemote;


@Component
public class CheckLoginInterceptor implements HandlerInterceptor{
//	@Autowired
//	private RestTemplateBuilder restBuilder;
//	@Autowired
//	private RestTemplate restTemplate;
	
	@Autowired
	private UserServiceRemote userRemote;

	public void afterCompletion(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, Exception arg3)
			throws Exception {
		// TODO Auto-generated method stub
		
	}
	
	public void postHandle(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, ModelAndView arg3)
			throws Exception {
		// TODO Auto-generated method stub
		
	}

	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		//获取请求带过来的token
		String token = request.getParameter("token");
		System.out.println("调用/user/token服务检查token是否合法"+token);
		response.setContentType("application/json;charset=UTF-8");
		
		
		if(token != null && !"".equals(token)){
			//调用/user/token服务检查token是否合法,合法就返回true,不合法返回false
//			RestTemplate restTemplate = restBuilder.build();
			//调用ovls_course_server的服务查询所有学科信息
//			ResponseResult userResult  = restTemplate.getForObject(
//				"http://USERSERVER/user/token?token="+token, ResponseResult.class);
			//利用Feign接口调用服务
			ResponseResult userResult = userRemote.checkToken(token);
			if(userResult.getStatus()==1){//通过验证，表示token合法
				return true;
			}
		}
		//未通过验证
		PrintWriter out = response.getWriter();
		out.println("{\"stauts\":2,\"msg\":\"不合法用户\"}");
		out.close();
		return false;
	}



}