package com.lj.ovls.paper.remote;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.lj.ovls.common.entity.ResponseResult;

@FeignClient(name="USERSERVER")//对应哪个服务实例
public interface UserServiceRemote {

	//调用/user/token服务
	@RequestMapping(value="/user/token",method=RequestMethod.GET)
	public ResponseResult checkToken(String token);
	
}
