package com.lj.ovls.paper.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lj.ovls.paper.dao.PaperMapper;
import com.lj.ovls.paper.entity.Paper;
import com.lj.ovls.common.entity.ResponseResult;

@Service
public class PaperServiceImpl implements PaperService {

	@Autowired
	private PaperMapper paperDao;
	
	@Override
	public ResponseResult loadSubjectPapers(int subjectId) {
		List<Paper> list = paperDao.selectBySubjectId(subjectId);
		ResponseResult result = new ResponseResult();
		if(list.isEmpty()){
			result.setStatus(2);
			result.setMsg("未找到数据");
		}else{
			result.setStatus(1);
			result.setMsg("查询成功");
			result.setData(list);
		}
		return result;
	}

}
