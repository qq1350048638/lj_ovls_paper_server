package com.lj.ovls.paper.service;

import com.lj.ovls.common.entity.ResponseResult;

public interface PaperService {
	
	public ResponseResult loadSubjectPapers(int subjectId);
	
}
