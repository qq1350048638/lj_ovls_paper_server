package com.lj.ovls.paper.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.lj.ovls.common.entity.ResponseResult;
import com.lj.ovls.paper.service.PaperService;

@RestController
public class PaperController {

	@Autowired
	private PaperService paperService;
	
	@RequestMapping(value="/paper/subject/{subjectId}",method=RequestMethod.GET)
	public ResponseResult loadSubjectPapers(
			@PathVariable("subjectId")int subjectId){
		return paperService.loadSubjectPapers(subjectId);
	}
	
}
